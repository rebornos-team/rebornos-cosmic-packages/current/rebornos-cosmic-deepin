# rebornos-cosmic-deepin

Deepin packages

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-cosmic-packages/current/rebornos-cosmic-deepin.git
```

**IMPORTANT:** Remember that ***qt5ct*** (the Qt settings application) must be uninstalled in Deepin so that there are no icon display problems.

